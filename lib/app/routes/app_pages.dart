import 'package:get/get.dart';

import '../modules/beranda/bindings/beranda_binding.dart';
import '../modules/beranda/views/beranda_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.LOGIN;

  static final routes = [
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.BERANDA,
      page: () => BerandaView(),
      binding: BerandaBinding(),
    ),
    // GetPage(
    //   name: _Paths.PRODUCT_LIST,
    //   page: () => ProductListView(),
    //   binding: ProductListBinding(),
    // ),
    // GetPage(
    //   name: _Paths.PRODUCT_DETAIL,
    //   page: () => ProductDetailView(),
    //   binding: ProductDetailBinding(),
    // ),
  ];
}

import 'package:shared_preferences/shared_preferences.dart';
SharedPreferences? preferences;

Future<void> initializePreference(String email) async{
  preferences = await SharedPreferences.getInstance();
  preferences?.setString("email", email);
  preferences?.setBool("islogin", true);
}

Future<bool> getIsLogin() async {
  preferences = await SharedPreferences.getInstance();
  bool? boolValue = preferences!.getBool('islogin');
  bool? CheckValue = preferences!.containsKey('islogin');
  if (CheckValue && boolValue!=null && boolValue){
    return true;
  }
  return false;
}

getEmail() async {
  preferences = await SharedPreferences.getInstance();
  String? email = preferences!.getString('email');
  return email;
}

clear() async{
  preferences = await SharedPreferences.getInstance();
  preferences!.clear();
}
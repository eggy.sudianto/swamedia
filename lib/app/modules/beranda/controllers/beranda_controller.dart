import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:swamedia/app/preferences.dart';

import '../model/employee.dart';


class BerandaController extends GetxController {
  var isLoading = true.obs;
  var employeeList = <Datum>[].obs;

  //TODO: Implement BerandaController
  var email = "".obs;
  final count = 0.obs;
  @override
  Future<void> onInit() async {
    super.onInit();
    email(await getEmail());
    _initList();
  }

  @override
  void onClose() {}

  Future<void> _initList() async {
    isLoading(true);

    final url = Uri.parse("https://63a167d8a543280f775561e5.mockapi.io/flutter");
    try {
      var request = http.Request('GET', url);
      var streamedResponse = await request.send();
      var response = await http.Response.fromStream(streamedResponse);

      final data = employeeFromJson(response.body);
      print(data);
      for (var datas in data.data) {
        employeeList.add(datas);
      }
    } catch (e) {
      print(e);
    }finally {
      isLoading(false);
    }
  }
}

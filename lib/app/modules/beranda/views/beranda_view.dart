import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/beranda_controller.dart';

class BerandaView extends GetView<BerandaController> {
  final BerandaController berandaController = Get.put(BerandaController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(() {
      if (berandaController.email=="") {
        return const Center(child: CircularProgressIndicator());
      } else {
        return Padding(
          padding: const EdgeInsets.only(top: 30, left: 20),
          child: Container(
            height: MediaQuery
                .of(context)
                .size
                .height,
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text("hi " + berandaController.email.toString()),
                // GridView.builder(
                //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                //         crossAxisCount: 1,
                //         childAspectRatio: MediaQuery.of(context).size.width /
                //             MediaQuery.of(context).size.height,
                //         crossAxisSpacing: 10,
                //         mainAxisSpacing: 5),
                //     itemCount: berandaController.employeeList.length,
                //     itemBuilder: (BuildContext ctx, index) {
                //       return InkWell(
                //         onTap: () {
                //           // berandaController.toProductDetail(
                //           //     berandaController
                //           //         .employeeList[index].id);
                //         },
                //         child: LayoutBuilder(builder: (ctx, constraints) {
                //           return Card(
                //             clipBehavior: Clip.antiAlias,
                //             elevation: 10,
                //             child: Padding(
                //               padding: const EdgeInsets.only(left: 15, right: 15, bottom: 10, top: 35),
                //               child: Column(
                //                 crossAxisAlignment: CrossAxisAlignment.start,
                //                 mainAxisAlignment: MainAxisAlignment.start,
                //                 children: [
                //                   Flexible(
                //                       child: Column(
                //                         crossAxisAlignment: CrossAxisAlignment.start,
                //                         mainAxisAlignment: MainAxisAlignment.start,
                //                         children: [
                //                           const SizedBox(height: 10,),
                //                           Text(berandaController.employeeList[index].name,
                //                             style: const TextStyle(color: Color(0xFF25315B), fontSize: 14),
                //                             textAlign: TextAlign.left,),
                //
                //                         ],
                //                       )
                //                   ),
                //
                //                 ],
                //               ),
                //             ),
                //           );
                //         }),
                //       );
                //     }),
              ],
            ),
          ),
        );
      }
      }),
    );
  }
}

// To parse this JSON data, do
//
//     final employee = employeeFromJson(jsonString);

import 'dart:convert';

Employee employeeFromJson(String str) => Employee.fromJson(json.decode(str));


class Employee {
  Employee({
    required this.message,
    required this.data,
    required this.count,
  });

  int message;
  List<Datum> data;
  int count;

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    count: json["count"],
  );
}

class Datum {
  Datum({
    required this.name,
    required this.nik,
    required this.position,
    required this.id,
  });

  String name;
  String nik;
  String position;
  String id;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    name: json["name"],
    nik: json["nik"],
    position: json["position"],
    id: json["id"],
  );
}

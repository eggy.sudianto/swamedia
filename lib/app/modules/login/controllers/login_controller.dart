import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../preferences.dart';

class LoginController extends GetxController {
  //TODO: Implement LoginController
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController TFEmail = TextEditingController();
  TextEditingController TFPassword = TextEditingController();

  final count = 0.obs;
  @override
  Future<void> onInit() async {
    super.onInit();
    if(await getIsLogin()){
      Get.toNamed("/beranda");
    }

    TFEmail.text = "fluttertest@swamedia.com";
    TFPassword.text = "fluttertest";
  }


  @override
  void dispose() {
    TFEmail.dispose();
    TFPassword.dispose();
    super.dispose();
  }


  @override
  void onClose() {}

  void validate(){
    if(formKey.currentState!.validate()){
      if(TFEmail.text=="fluttertest@swamedia.com" && TFPassword.text == "fluttertest"){
        initializePreference(
          TFEmail.text,
        );
        Get.toNamed("/beranda");
      }else{
        Get.snackbar(
          "Error",
          "Email / Password yang dimasukan salah",
          colorText: Colors.white,
          backgroundColor: Colors.lightBlue,
          icon: const Icon(Icons.add_alert),
        );
      }
    }
  }
}

import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  final LoginController loginController = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Container(
                height: MediaQuery.of(context).size.height,
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Form(
                      key: loginController.formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 50),
                            child: TextFormField(
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              controller: loginController.TFEmail,
                              style: const TextStyle(
                                  color: Colors.black
                              ),
                              decoration: const InputDecoration(
                                hintStyle: TextStyle(
                                    color: Colors.black
                                ),
                                border: OutlineInputBorder(),
                                labelStyle: TextStyle(color: Colors.grey),
                                labelText: 'Emaill',),
                              validator: RequiredValidator(errorText: "Email Must be filled"),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 50),
                            child: TextFormField(
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              controller: loginController.TFPassword,
                              style: const TextStyle(
                                  color: Colors.black
                              ),
                              decoration: const InputDecoration(
                                hintStyle: TextStyle(
                                    color: Colors.black
                                ),
                                border: OutlineInputBorder(),
                                labelStyle: TextStyle(color: Colors.grey),
                                labelText: 'Password',),
                              validator: RequiredValidator(errorText: "Password Must be filled"),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 30,),
                    Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 50),
                          child: InkWell(
                              onTap: loginController.validate,
                              child: const Text("Login")
                          ),
                        )
                    ),
                  ],
                ),
              ),
            ),

    );
  }
}
